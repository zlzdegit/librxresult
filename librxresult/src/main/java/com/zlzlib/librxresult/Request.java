package com.zlzlib.librxresult;

import android.content.Intent;

import io.reactivex.rxjava3.annotations.Nullable;


/**
 * @Desc: 传递数据的类
 * @Copyright:
 * @DateTime: 2020/9/17 17:17
 * @Author zlz
 * @Version 1.0
 */
class Request {

    private final Intent intent;
    private OnPreResult onPreResult;
    private OnResult onResult;

    public Request(@Nullable Intent intent) {
        this.intent = intent;
    }

    void setOnPreResult(@Nullable OnPreResult onPreResult) {
        this.onPreResult = onPreResult;
    }

    OnPreResult onPreResult() {
        return this.onPreResult;
    }

    public void setOnResult(OnResult onResult) {
        this.onResult = onResult;
    }

    public OnResult onResult() {
        return this.onResult;
    }

    @Nullable
    public Intent intent() {
        return this.intent;
    }
}
