package com.zlzlib.librxresult;

import android.content.Intent;

/**
 * @Desc: 上个页面返回的值
 * @Copyright:
 * @DateTime: 2020/9/17 17:06
 * @Author zlz
 * @Version 1.0
 */
public class RxResult<T> {

    //发起跳转的页面
    private final T targetUI;
    //结果返回码
    private final int resultCode;
    //请求码
    private final int requestCode;
    //包含数据的intent
    private final Intent data;

    public RxResult(T targetUI, int requestCode, int resultCode, Intent data) {
        this.targetUI = targetUI;
        this.resultCode = resultCode;
        this.requestCode = requestCode;
        this.data = data;
    }

    public T targetUI() {
        return targetUI;
    }

    public int resultCode() {
        return resultCode;
    }

    public int requestCode() {
        return requestCode;
    }

    public Intent data() {
        return data;
    }
}
