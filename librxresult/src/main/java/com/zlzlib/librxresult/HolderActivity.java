package com.zlzlib.librxresult;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;


/**
 * @Desc: 用于接收数据的透明的activity
 * @Copyright:
 * @DateTime: 2020/9/18 9:28
 * @Author zlz
 * @Version 1.0
 */
public class HolderActivity extends Activity {
    private static Request request;
    private OnPreResult onPreResult;
    private OnResult onResult;
    private int resultCode;
    private int requestCode;
    private Intent data;
    //失败返回的
    private static final int FAILED_REQUEST_CODE = -999;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (request == null) {
            this.finish();
        } else {
            this.onPreResult = request.onPreResult();
            this.onResult = request.onResult();
            if (savedInstanceState == null) {
                if (request instanceof RequestIntentSender) {
                    RequestIntentSender requestIntentSender = (RequestIntentSender) request;
                    if (requestIntentSender.getOptions() == null) {
                        this.startIntentSender(requestIntentSender);
                    } else {
                        this.startIntentSenderWithOptions(requestIntentSender);
                    }
                } else {
                    try {
                        this.startActivityForResult(request.intent(), 0);
                    } catch (ActivityNotFoundException var3) {
                        if (this.onResult != null) {
                            this.onResult.error(var3);
                        }
                    }
                }
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.resultCode = resultCode;
        this.requestCode = requestCode;
        this.data = data;
        if (this.onPreResult != null) {
            this.onPreResult.response(requestCode, resultCode, data).doOnComplete(HolderActivity.this::finish).subscribe();
        } else {
            this.finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (this.onResult != null) {
            this.onResult.response(this.requestCode, this.resultCode, this.data);
        }
    }

    private void startIntentSender(RequestIntentSender requestIntentSender) {
        try {
            this.startIntentSenderForResult(requestIntentSender.getIntentSender(), 0,
                    requestIntentSender.getFillInIntent(), requestIntentSender.getFlagsMask(),
                    requestIntentSender.getFlagsValues(), requestIntentSender.getExtraFlags());
        } catch (IntentSender.SendIntentException var3) {
            var3.printStackTrace();
            this.onResult.response(FAILED_REQUEST_CODE, 0, (Intent) null);
        }

    }

    private void startIntentSenderWithOptions(RequestIntentSender requestIntentSender) {
        try {
            this.startIntentSenderForResult(requestIntentSender.getIntentSender(), 0,
                    requestIntentSender.getFillInIntent(), requestIntentSender.getFlagsMask(),
                    requestIntentSender.getFlagsValues(), requestIntentSender.getExtraFlags(), requestIntentSender.getOptions());
        } catch (IntentSender.SendIntentException var3) {
            var3.printStackTrace();
            this.onResult.response(FAILED_REQUEST_CODE, 0, null);
        }

    }

    static void setRequest(Request aRequest) {
        request = aRequest;
    }
}