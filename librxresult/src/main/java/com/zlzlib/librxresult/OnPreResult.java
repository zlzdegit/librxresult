package com.zlzlib.librxresult;

import android.content.Intent;

import io.reactivex.rxjava3.annotations.Nullable;
import io.reactivex.rxjava3.core.Observable;


/**
 * @Desc:
 * @Copyright:
 * @DateTime: 2020/9/17 17:15
 * @Author zlz
 * @Version 1.0
 */
public interface OnPreResult<T> {

    Observable<T> response(int var1, int var2, @Nullable Intent var3);

}
