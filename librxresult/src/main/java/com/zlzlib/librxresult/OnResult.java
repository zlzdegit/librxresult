package com.zlzlib.librxresult;

import android.content.Intent;

import java.io.Serializable;

import io.reactivex.rxjava3.annotations.Nullable;


/**
 * @Desc:
 * @Copyright:
 * @DateTime: 2020/9/17 17:16
 * @Author zlz
 * @Version 1.0
 */
public interface OnResult extends Serializable {
    void response(int var1, int var2, @Nullable Intent var3);

    void error(Throwable var1);
}
