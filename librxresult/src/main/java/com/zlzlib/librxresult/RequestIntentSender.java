package com.zlzlib.librxresult;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;

import io.reactivex.rxjava3.annotations.Nullable;


/**
 * @Desc:
 * @Copyright:
 * @DateTime: 2020/9/17 17:21
 * @Author zlz
 * @Version 1.0
 */
class RequestIntentSender extends Request {
    private final IntentSender intentSender;
    private final Intent fillInIntent;
    private final int flagsMask;
    private final int flagsValues;
    private final int extraFlags;
    private final Bundle options;

    public RequestIntentSender(IntentSender intentSender, @Nullable Intent fillInIntent,
                               int flagsMask, int flagsValues, int extraFlags, @Nullable Bundle options) {
        super((Intent) null);
        this.intentSender = intentSender;
        this.fillInIntent = fillInIntent;
        this.flagsMask = flagsMask;
        this.flagsValues = flagsValues;
        this.extraFlags = extraFlags;
        this.options = options;
    }

    public IntentSender getIntentSender() {
        return this.intentSender;
    }

    public Intent getFillInIntent() {
        return this.fillInIntent;
    }

    public int getFlagsMask() {
        return this.flagsMask;
    }

    public int getFlagsValues() {
        return this.flagsValues;
    }

    public int getExtraFlags() {
        return this.extraFlags;
    }

    public Bundle getOptions() {
        return this.options;
    }
}
