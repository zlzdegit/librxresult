package com.zlzlib.librxresult;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.functions.Predicate;


/**
 * @Desc:页面的生命周期回调
 * @Copyright:
 * @DateTime: 2020/9/17 17:23
 * @Author zlz
 * @Version 1.0
 */
class ActivitiesLifecycleCallbacks {
    final Application application;
    //当前第一个存在的activity
    volatile Activity liveActivityOrNull;
    Application.ActivityLifecycleCallbacks activityLifecycleCallbacks;
    volatile boolean emitted = false;

    public ActivitiesLifecycleCallbacks(Application application) {
        this.application = application;
        this.registerActivityLifeCycle();
    }

    //注册生命周期监听回调
    private void registerActivityLifeCycle() {
        if (this.activityLifecycleCallbacks != null) {
            this.application.unregisterActivityLifecycleCallbacks(this.activityLifecycleCallbacks);
        }

        this.activityLifecycleCallbacks = new Application.ActivityLifecycleCallbacks() {
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                ActivitiesLifecycleCallbacks.this.liveActivityOrNull = activity;
            }

            public void onActivityStarted(Activity activity) {
            }

            public void onActivityResumed(Activity activity) {
                ActivitiesLifecycleCallbacks.this.liveActivityOrNull = activity;
            }

            public void onActivityPaused(Activity activity) {
                ActivitiesLifecycleCallbacks.this.liveActivityOrNull = null;
            }

            public void onActivityStopped(Activity activity) {
            }

            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            }

            public void onActivityDestroyed(Activity activity) {
            }
        };
        this.application.registerActivityLifecycleCallbacks(this.activityLifecycleCallbacks);
    }

    Activity getLiveActivity() {
        return this.liveActivityOrNull;
    }

    Observable<Activity> getOLiveActivity() {
        this.emitted = false;
        //间隔50毫秒发送
        return Observable.interval(50L, 50L, TimeUnit.MILLISECONDS).map(new Function<Long, Object>() {
            public Object apply(Long aLong) throws Exception {
                return ActivitiesLifecycleCallbacks.this.liveActivityOrNull == null ? 0 :
                        ActivitiesLifecycleCallbacks.this.liveActivityOrNull;
            }
        }).takeWhile(new Predicate<Object>() {
            public boolean test(Object candidate) throws Exception {
                boolean continueEmitting = true;
                if (ActivitiesLifecycleCallbacks.this.emitted) {
                    continueEmitting = false;
                }
                if (candidate instanceof Activity) {
                    ActivitiesLifecycleCallbacks.this.emitted = true;
                }
                //为true时往下进行
                return continueEmitting;
            }
        }).filter(new Predicate<Object>() {
            public boolean test(Object candidate) throws Exception {
                return candidate instanceof Activity;
            }
        }).map(new Function<Object, Activity>() {
            public Activity apply(Object activity) throws Exception {
                return (Activity) activity;
            }
        });
    }
}
